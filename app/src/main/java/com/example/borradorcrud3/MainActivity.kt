package com.example.borradorcrud3

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.borradorcrud3.model.Alumno
import com.example.borradorcrud3.ui.AddAlumnoActivity
import com.example.borradorcrud3.ui.EditAlumnoActivity
import com.example.borradorcrud3.ui.AlumnoAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {
    private lateinit var adapter: AlumnoAdapter

    private val appRepository by lazy {
        (application as AlumnoApplication).provideRepository()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //appRepository = Injector.provideAlumnoRepository()
        ui()
        //loadNotes()
    }

    override fun onResume() {
        super.onResume()
        loadAlu()
    }

    private fun ui(){
        recyclerView.layoutManager = LinearLayoutManager(this)

        adapter = AlumnoAdapter(emptyList()) { itAlumno->
            showMessage("item ${itAlumno.name}")
            goToAlumno(itAlumno)
        }

        recyclerView.adapter = adapter

        floatingActionButton.setOnClickListener {
            goToAddAlumno()
        }
    }

    private fun goToAddAlumno() {

        startActivity(Intent(this, AddAlumnoActivity::class.java))
    }



    private fun goToAlumno(alumno: Alumno) {

        val bundle= Bundle()
        bundle.putSerializable("ALUMNO",alumno)
        val intent= Intent(this, EditAlumnoActivity::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    private fun showMessage(message: String) {
        Toast.makeText(this, "item $message", Toast.LENGTH_SHORT).show()
    }


    private fun loadAlu(){
        appRepository.getAll {
            adapter.update(it)
    }

}}
