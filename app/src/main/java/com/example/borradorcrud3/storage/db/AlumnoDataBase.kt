package com.example.borradorcrud3.storage.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [DBAlumno::class], version = 1)
abstract class AlumnoDataBase : RoomDatabase() {

    abstract fun alumnoDao(): AlumnoDao

    companion object {
        private var INSTANCE: AlumnoDataBase? = null
        private const val DBNAME="BDRoom.db"

        fun getInstance(context: Context): AlumnoDataBase? {
            if (INSTANCE == null) {
                synchronized(AlumnoDataBase::class) {
                    INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            AlumnoDataBase::class.java, DBNAME
                    )
                            .build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}