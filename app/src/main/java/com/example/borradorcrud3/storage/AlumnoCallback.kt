package com.example.borradorcrud3.storage

import androidx.annotation.MainThread

interface AlumnoCallback<T> {

    @MainThread
    fun success(data:List<T>)

    @MainThread
    fun success(){}

    @MainThread
    fun error (exception: Exception?)
}