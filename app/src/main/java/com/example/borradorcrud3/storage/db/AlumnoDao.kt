package com.example.borradorcrud3.storage.db

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.OnConflictStrategy.IGNORE


@Dao
interface AlumnoDao {

    @Query("SELECT * from tb_alumnos")
    fun alumnos(): List<DBAlumno>

    @Insert(onConflict = REPLACE)
    fun addAlumno(alumno: DBAlumno)

    //@Insert(onConflict = IGNORE)
    //fun insertOrReplaceAlumnos(alumno: Alumno)

    @Update(onConflict = REPLACE)
    fun updateAlumno(alumno: DBAlumno)

    @Delete
    fun deleteAlumno(alumno: DBAlumno)

    @Query("DELETE from tb_alumnos")
    fun deleteAll()
}