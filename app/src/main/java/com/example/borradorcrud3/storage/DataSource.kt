package com.example.borradorcrud3.storage

import com.example.borradorcrud3.storage.db.DBAlumno



interface DataSource {

    fun alumnos():List<DBAlumno>
    fun addAlumno(alumno: DBAlumno)
    fun updateAlumno(alumno: DBAlumno)
    fun deleteAlumno(alumno: DBAlumno)
}