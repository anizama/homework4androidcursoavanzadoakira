package com.example.borradorcrud3.storage

import com.example.borradorcrud3.model.Alumno
import com.example.borradorcrud3.storage.db.DBAlumno


/**
 * https://kotlinlang.org/docs/reference/collection-transformations.html
 * http://modelmapper.org/
 */
object Mapper {

    fun alumnoDbToAlumno(dbAlumno: DBAlumno): Alumno {
        return Alumno(dbAlumno.id,dbAlumno.name,dbAlumno.edad,dbAlumno.peso,dbAlumno.pesoideal)
    }

    fun alumnoToDbAlumno(alumno: Alumno): DBAlumno {
        return DBAlumno(alumno.id, alumno.name,alumno.edad,alumno.peso,alumno.pesoideal)
    }

    fun mapList(alumnoList: List<Alumno>):List<DBAlumno>{
        return alumnoList.map { alumnoToDbAlumno(it) }
    }

    fun mapDbList(alumnoDBList: List<DBAlumno>):List<Alumno>{
        return alumnoDBList.map { alumnoDbToAlumno(it) }
    }
}