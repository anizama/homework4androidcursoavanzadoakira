package com.example.borradorcrud3.storage.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "tb_alumnos")
data class DBAlumno(@PrimaryKey(autoGenerate = true)val id:Int?,
                        @ColumnInfo(name = "name") val name:String?,
                        @ColumnInfo(name = "edad") val edad:String?,
                        @ColumnInfo(name = "peso") val peso:String?,
                        @ColumnInfo(name = "pesoideal") val pesoideal:String?)

{

    override fun toString(): String {
        return "DBAlumno(id=$id, name=$name, edad=$edad,peso=$peso,pesoideal=$pesoideal)"
    }
}