package com.example.borradorcrud3

import android.app.Application
import com.example.borradorcrud3.model.AlumnoRepository

class AlumnoApplication:Application() {
    private lateinit var repository: AlumnoRepository
    override fun onCreate() {
        super.onCreate()

        repository = AlumnoRepository(
                AppExecutors(),
                DBDataSource(this))
    }
    fun provideRepository()= repository
}