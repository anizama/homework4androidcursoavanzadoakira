package com.example.borradorcrud3.model

import java.io.Serializable

data class Alumno(val id:Int?, val name:String?, val edad: String?, val peso: String?,val pesoideal:String?):Serializable{

    override fun toString(): String {
        return "Alumno(id=$id, name=$name, edad=$edad, peso=$peso,pesoideal=$pesoideal)"
    }


}