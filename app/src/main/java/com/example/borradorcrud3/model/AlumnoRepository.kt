package com.example.borradorcrud3.model

import com.example.borradorcrud3.AppExecutors
import com.example.borradorcrud3.storage.DataSource
import com.example.borradorcrud3.storage.AlumnoCallback
import com.example.borradorcrud3.storage.Mapper
import com.example.borradorcrud3.storage.db.DBAlumno

class AlumnoRepository(private val appExecutors: AppExecutors,
                       private val dataSource: DataSource
) {

    fun getAllWithCallback(callback:AlumnoCallback<Alumno>){
        appExecutors.diskIO.execute {
            val alumnoList = dataSource.alumnos().map {
                Alumno(it.id, it.name, it.edad,it.peso,it.pesoideal)
            }
            appExecutors.mainThread.execute {
                callback.success(alumnoList)
            }
        }
    }

    fun getAll(callback: (alumnos: List<Alumno>) -> Unit) {
        appExecutors.diskIO.execute {
            val alumnoList = dataSource.alumnos().map {
                Alumno(it.id, it.name, it.edad,it.peso,it.pesoideal)
            }
            appExecutors.mainThread.execute {
                callback(alumnoList)
            }
        }
    }

    fun add(alumno: Alumno, callback: () -> Unit) {
        appExecutors.diskIO.execute {
            dataSource.addAlumno(Mapper.alumnoToDbAlumno(alumno))
            appExecutors.mainThread.execute {
                callback()
            }
        }
    }

    fun update(alumno: Alumno) {
        appExecutors.diskIO.execute {
            dataSource.updateAlumno(Mapper.alumnoToDbAlumno(alumno))
        }
    }

    fun delete(alumno: Alumno) {
        appExecutors.diskIO.execute {
            dataSource.deleteAlumno(Mapper.alumnoToDbAlumno(alumno))
        }
    }

}