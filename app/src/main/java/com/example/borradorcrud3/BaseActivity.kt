package com.example.borradorcrud3

import androidx.appcompat.app.AppCompatActivity
import com.example.borradorcrud3.model.AlumnoRepository

abstract class BaseActivity:AppCompatActivity() {

    /*protected val repository by lazy {
        NoteRepository(AppExecutors(),
            DBDataSource(this))
    }*/

    protected val repository by lazy {
        AlumnoRepository(AppExecutors(),
            DBDataSource(applicationContext))
    }
}