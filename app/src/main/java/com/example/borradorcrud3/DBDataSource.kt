package com.example.borradorcrud3

import android.content.Context
import com.example.borradorcrud3.storage.DataSource
import com.example.borradorcrud3.storage.db.AlumnoDao
import com.example.borradorcrud3.storage.db.AlumnoDataBase
import com.example.borradorcrud3.storage.db.DBAlumno


class DBDataSource(context:Context):DataSource{

    private lateinit var alumnoDao:AlumnoDao

    init{
        val db = AlumnoDataBase.getInstance(context)
        db?.let {
            alumnoDao = it.alumnoDao()
        }
    }

  /*  override fun notes(): List<DBNote> = noteDao.notes()

    override fun addNote(note: DBNote) {
        noteDao.addNote(note)
    }

    override fun updateNote(note: DBNote) {
        noteDao.updateNote(note)
    }

    override fun deleteNote(note: DBNote) {
        noteDao.deleteNote(note)
    }
*/
    override fun alumnos(): List<DBAlumno> = alumnoDao.alumnos()

    override fun addAlumno(alumno: DBAlumno) {
        alumnoDao.addAlumno(alumno)
    }

    override fun updateAlumno(alumno: DBAlumno) {
        alumnoDao.updateAlumno(alumno)
    }

    override fun deleteAlumno(alumno: DBAlumno) {
       alumnoDao.deleteAlumno(alumno)
    }

}