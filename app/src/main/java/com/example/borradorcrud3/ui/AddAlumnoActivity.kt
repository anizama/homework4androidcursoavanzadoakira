package com.example.borradorcrud3.ui

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.borradorcrud3.BaseActivity
//import com.emedinaa.kotlinapp.R
import com.example.borradorcrud3.R
import com.example.borradorcrud3.model.Alumno
import com.example.borradorcrud3.model.AlumnoRepository
import com.example.borradorcrud3.storage.Mapper
import kotlinx.android.synthetic.main.activity_add_alumno.*
import kotlinx.android.synthetic.main.activity_add_alumno.btnAddAlu
import kotlinx.android.synthetic.main.activity_add_alumno.eteNameAlu

class AddAlumnoActivity :BaseActivity() {

    //private lateinit var alumnoRepository: AlumnoRepository

    private var name: String? = null
    private var edad: String? = null
    private var peso: String? = null
    private var pesoideal:String?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_alumno)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        //   setupRepository()
        ui()
    }


    private fun ui() {
        btnAddAlu.setOnClickListener {
            if (validateForm()) {
                addAlumno()
                finish()
            }
        }
    }

    private fun validateForm(): Boolean {
        clearForm()
        name = eteNameAlu.text.toString().trim()
        edad = eteEdadAlu.text.toString().trim()
        peso = etePesoAlu.text.toString().trim()

        if (peso.toString().toInt() in 60..69 ) txtpesoidealAlu.text = "Si esta apto" else txtpesoidealAlu.text = "No esta apto"
        if (edad.toString().toInt() < 17) Toast.makeText(this, "Edad incorrecta", Toast.LENGTH_SHORT).show()
        if (name.isNullOrEmpty()) {
            eteNameAlu.error = "Campo nombre inválido"
            return false
        }

        if (edad.isNullOrEmpty()) {
            eteNameAlu.error = "Campo edad inválido"
            return false
        }

        if (peso.isNullOrEmpty()) {
            etePesoAlu.error = "Campo peso inválido"
            return false
        }

        return true
    }

    private fun clearForm() {
        eteNameAlu.error = null
        eteEdadAlu.error = null
        etePesoAlu.error = null
        txtpesoidealAlu.error=null
    }

    private fun addAlumno() {
        val alumno = Alumno(null, name, edad, peso,pesoideal)
        repository.add(alumno) {
            Toast.makeText(this, "Correcto", Toast.LENGTH_SHORT).show()
        }


    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}