package com.example.borradorcrud3.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
//import com.emedinaa.kotlinapp.R
import com.example.borradorcrud3.R
import com.example.borradorcrud3.model.Alumno
//import com.example.borradorcrud3.model.Note
import kotlinx.android.synthetic.main.row_note.view.*

class  AlumnoAdapter(private var alumnos:List<Alumno>,
                     private val itemCallback:(alumno: Alumno)->Unit?):RecyclerView.Adapter<
        AlumnoAdapter.AlumnoViewHolder>(){


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlumnoViewHolder {
        //pintar
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.row_alumno,
            parent,false)
        return AlumnoViewHolder(view)
    }

    override fun getItemCount(): Int {
        return alumnos.size
    }

    override fun onBindViewHolder(holder: AlumnoViewHolder, position: Int) {
        holder.bind(alumnos[position])
        holder.view.setOnClickListener {
            itemCallback(alumnos[position])
        }
    }

    fun update(data:List<Alumno>){
        alumnos= data
        notifyDataSetChanged()
        //DiffUtils
    }

    class AlumnoViewHolder(val view:View):RecyclerView.ViewHolder(view){

        fun bind(alumno: Alumno){
            view.tviName.text = alumno.name
        }
    }
}