package com.example.borradorcrud3.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.borradorcrud3.BaseActivity
//import com.emedinaa.kotlinapp.R
import com.example.borradorcrud3.R
import com.example.borradorcrud3.model.Alumno
import com.example.borradorcrud3.model.AlumnoRepository
import com.example.borradorcrud3.storage.Mapper
import kotlinx.android.synthetic.main.activity_add_alumno.eteEdadAlu
import kotlinx.android.synthetic.main.activity_add_alumno.etePesoAlu
import kotlinx.android.synthetic.main.activity_edit_alumno.*

class EditAlumnoActivity : BaseActivity(), AlumnoDialogFragment.DialogListener {

    private lateinit var alumnoRepository: AlumnoRepository
    private var alumni: Alumno?=null
    private var name:String?=null
    private var edad:String?=null
    private var peso:String?=null
    private var pesoideal:String?=null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_alumno)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        verifyExtras()
        //setupRepository()
        populate()

        ui()
    }

  //  private fun setupRepository(){
    //    alumnoRepository= Injector.provideAlumnoRepository()
    //}

    private fun ui(){
        btnEditNote.setOnClickListener {
            if(validateForm()){
                editAlu()
            }
        }

        btnDeleteNote.setOnClickListener {
            showAluDialog()
        }
    }

    private fun editAlu(){
        val noteId= alumni?.id
        val nAlumno= Alumno(noteId,name,edad,peso,pesoideal)
        repository.update(nAlumno)
        finish()
    }

    private fun validateForm():Boolean{
        name= eteNameAlu.text.toString()
        edad= eteEdadAlu.text.toString()
        peso=etePesoAlu.text.toString()
        pesoideal=txtpesoidealAlu.text.toString()
        if(name.isNullOrEmpty()){
            return false
        }
        if(edad.isNullOrEmpty()){
            return false
        }
        if (peso.isNullOrEmpty()){
            return false
        }

        return true
    }

    private fun populate(){
        alumni?.let {
            eteNameAlu.setText(it.name)
            eteEdadAlu.setText(it.edad)
            etePesoAlu.setText(it.peso)
            txtpesoidealAlu.text = it.pesoideal
        }
    }

    private fun showAluDialog(){
        val noteDialogFragment= AlumnoDialogFragment()
        val bundle= Bundle()
        bundle.putString("TITLE","¿Deseas eliminar este alumno?")
        bundle.putInt("TYPE",100)

        noteDialogFragment.arguments= bundle
        noteDialogFragment.show(supportFragmentManager,"dialog")
    }

    override fun onPositiveListener(any: Any?, type: Int) {
        alumni?.let {
            repository.delete(it)
        }
        finish()
    }

    override fun onNegativeListener(any: Any?, type: Int) {}

    private fun verifyExtras(){
        intent?.extras?.let {
            alumni= it.getSerializable("ALUMNO") as Alumno
        }
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
